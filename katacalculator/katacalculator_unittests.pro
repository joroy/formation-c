TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CFLAGS += -std=gnu99

SOURCES += main.c \
    calculator_kata.c

HEADERS += \
    calculator_kata.h

