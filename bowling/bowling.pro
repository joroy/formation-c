TEMPLATE = app
CONFIG += console
CONFIG -= app_bundle
CONFIG -= qt

QMAKE_CFLAGS += -std=gnu99

SOURCES += main.c \
    bowling_game.c

HEADERS += \
    bowling_game.h

